<?php

namespace Facade;

class DB{
    var $conn;

    public function __construct(){
        $conn = new \PDO("mysql:host=localhost;dbname=northwind","root","P@ssw0rd");
        $this->conn = $conn;
    }


}