<?php

namespace Model;

class ApiResponse{
    public $meta;
    public $data;

    public function __construct($success, $message, $status, $data){
        $timstamp = time();
        $this->meta = [
            "success" => $success,
            "status" => $status,
            "message" => $message,
            "timestamp" => $timstamp
        ];
        $this->data = $data;
    }

    public static function ok($message, $data){
        return new ApiResponse(true, $message, 200, $data);
    }

    public static function created($message, $data){
        return new ApiResponse(true, $message, 201, $data);
    }

    public static function saved($message, $data){
        return new ApiResponse(true, $message, 200, $data);
    }

    public static function accepted($message, $data){
        return new ApiResponse(true, $message, 202, $data);
    }
    public static function noContent($message, $data){
        return new ApiResponse(true, $message, 204, $data);
    }

    public static function found($message, $data){
        return new ApiResponse(true, $message, 302, $data);
    }

    public static function badRequest($message, $data){
        return new ApiResponse(true, $message, 400, $data);
    }

    public static function unAuthorize($message, $data){
        return new ApiResponse(true, $message, 401, $data);
    }

    public static function notFound($message, $data){
        return new ApiResponse(true, $message, 404, $data);
    }

    public static function notAcceptable($message, $data){
        return new ApiResponse(true, $message, 406, $data);
    }

    public static function internalServerError($message, $data){
        return new ApiResponse(true, $message, 500, $data);
    }
}