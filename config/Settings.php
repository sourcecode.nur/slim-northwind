<?php

return [
    "settings"=>[
        "displayErrorDetails"=>true,
        "logger"=>[
            "name" => "slim-northwind",
            "path" => __DIR__ . "/../logs/app.log",
            "level" => \Monolog\Logger::DEBUG
        ]
    ]
];