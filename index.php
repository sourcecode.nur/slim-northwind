<?php

require __DIR__. "/vendor/autoload.php";

$settings = require __DIR__. "/config/Settings.php";

use Slim\Http\Response as Response;
use Model\ApiResponse as ApiResponse;
use Facade\DB as DB;

$app = new \Slim\App($settings);
require __DIR__. "/config/Dependency.php";
require __DIR__. "/config/Middleware.php";  
 
$app->get("/", function($req, Response $res, $args){
    $data = ["meta"=>"ok", "data"=>"Slim run success!"];
    
    $res = $res->withJson(ApiResponse::ok("success", $data));  
    return $res;
});

$app->post("/salam", function($req, $res, $args){
    $data = $req->getParsedBody();
    $nama = $data["nama"];
    $jurusan = $data["jurusan"];
    $res->getBody()->write("<h1>Hello $nama</h1>"
    ."<h2>Jurusan anda adalah: $jurusan</h2>");
});

$app->post("/salam/[{reqid}]", function($req, $res, $args){
    $data = $req->getParsedBody();
    $nama = $data["nama"];
    $jurusan = $data["jurusan"];
    $reqid = $args["reqid"];
    $res->getBody()->write("<h1>Hello $nama</h1>"
    ."<h2>Jurusan anda adalah: $jurusan</h2>"
    ."<h3>RequestID = $reqid</h3>");
});

$app->get("/customers", function($req, $res, $args){
    $db = new DB();
    $stm = $db->conn->prepare("SELECT * FROM Customers");
    $stm->execute();
    $rows = $stm->fetchAll(PDO::FETCH_OBJ);
    $res = $res->withJson(ApiResponse::ok("success", $rows));  
    return $res;
});

$app->get("/customers/[{id}]", function($req, $res, $args){ 
    $id = $args["id"];
    $db = new DB();
    $stm = $db->conn->prepare("SELECT * FROM Customers WHERE CustomerID = :id");
    //$stm->bindParam(":id", $id, PDO::PARAM_STR);
    $stm->execute([":id"=>$id]);
    $row = $stm->fetchObject();
    $res = $res->withJson(ApiResponse::ok("success", $row));  
    return $res;
});

//insert customers
$app->post("/customers", function($req, $res, $args){
    $data = $req->getParsedBody();
    $db = new DB();
    $sql = "INSERT INTO Customers VALUES (?,?,?,?,?,?,?,?,?,?,?)";
    $stm = $db->conn->prepare($sql);
    $stm->bindValue(1, $data["CustomerID"]);
    $stm->bindValue(2, $data["CompanyName"]);
    $stm->bindValue(3, $data["ContactName"]);
    $stm->bindValue(4, $data["ContactTitle"]);
    $stm->bindValue(5, $data["Address"]);
    $stm->bindValue(6, $data["City"]);
    $stm->bindValue(7, $data["Region"]);
    $stm->bindValue(8, $data["Country"]);
    $stm->bindValue(9, $data["PostalCode"]);
    $stm->bindValue(10, $data["Phone"]);
    $stm->bindValue(11, $data["Fax"]);
    $stm->execute(); 
    $res = $res->withJson(ApiResponse::ok("success", $res));  
    return $res;
});

//update customers
$app->put("/customers", function($req, $res, $args){
    $data = $req->getParsedBody();
    $db = new DB();
    $sql = "UPDATE Customers SET CompanyName=?,ContactName=?,ContactTitle=?,
    Address=?, City=?, Region=?, Country=?,PostalCode=?, Phone=?, Fax=? WHERE CustomerID = ?";
    $stm = $db->conn->prepare($sql);
    $stm->bindValue(1, $data["CompanyName"]);
    $stm->bindValue(2, $data["ContactName"]);
    $stm->bindValue(3, $data["ContactTitle"]);
    $stm->bindValue(4, $data["Address"]);
    $stm->bindValue(5, $data["City"]);
    $stm->bindValue(6, $data["Region"]);
    $stm->bindValue(7, $data["Country"]);
    $stm->bindValue(8, $data["PostalCode"]);
    $stm->bindValue(9, $data["Phone"]);
    $stm->bindValue(10, $data["Fax"]);
    $stm->bindValue(11, $data["CustomerID"]);
    $stm->execute(); 
    $res = $res->withJson(ApiResponse::ok("success", "Data updated"));  
    return $res;
});

//delete customers
$app->delete("/customers/{id}", function($req, $res, $args){
    $id = $args["id"];
    $db = new DB();
    $sql = "DELETE FROM Customers WHERE CustomerID = ?";
    $stm = $db->conn->prepare($sql); 
    $stm->bindValue(1, $id);
    $stm->execute(); 
    $res = $res->withJson(ApiResponse::ok("success", "Data Deleted"));  
    return $res;
});
 
$app->run();